import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import { Layout } from "../components/Layout.jsx"
import { SEO } from "../components/SEO.jsx"
import { useContent } from "react-ssg";
import styles from "./IndexPage.module.css";
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useInView } from "react-intersection-observer";
import Typed from 'react-typed';
import { SectionYml } from "../components/SectionYml.jsx";
import { CardColumns } from "react-bootstrap";
import { ItemCard } from "../components/item/ItemCard.jsx";
import { CardGroup } from "react-bootstrap";
import { CardDeck } from "react-bootstrap";
import { indexMarkdownFolder } from "../util/indexFolder.js";

export const IndexPage = () => {
  const db = useContent();
  const data = db['/home.yml'];
  const items = indexMarkdownFolder(db, 'items');
  return (
    <Layout>
      <SEO title={data.header.name}/>
      <h1>فهرست محصولات</h1>
      <CardColumns style={{ columnCount: 2 }}>
        {items.map((k)=>{
          return (<ItemCard {...k} id={k.url.slice(7,-1)}/>);
        })}
      </CardColumns>
    </Layout>
  );
};
