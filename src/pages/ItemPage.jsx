import React, { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"

import { Layout } from "../components/Layout.jsx"
import { SEO } from "../components/SEO.jsx"
import { useContent } from "react-ssg";
import { HtmlElement } from "../components/HtmlElement.jsx";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { ItemCard } from "../components/item/ItemCard.jsx";
import { NotFoundPage } from "./NotFoundPage.jsx";
import { Button } from "react-bootstrap";
import { RenderPrice } from "../components/item/RenderPrice.jsx";

export const ItemPage = () => {
  const data = useContent('/home.yml');
  const { id } = useParams('id');
  const d = useContent(`/items/${id}.md`);
  if (!d) {
    return <NotFoundPage/>;
  }
  return (
    <Layout>
      <SEO title={data.header.name}/>
      <Row>
        <Col md={9}>
          <h1>{d.frontmatter.name}</h1>
          <HtmlElement content={d.html}/>
          <Button as={Link} to="/buy/">خرید</Button>
          <h1>ریز قیمت</h1>
          <RenderPrice obj={d.frontmatter.price}/>
        </Col>
        <Col md={3}>
          <ItemCard {...d.frontmatter} id={id}/>
        </Col>
      </Row>
    </Layout>
  );
};
