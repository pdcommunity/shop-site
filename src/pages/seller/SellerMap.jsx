import React from "react"

import { Layout } from "../../components/Layout.jsx"
import { SEO } from "../../components/SEO.jsx"
import { useContent } from "react-ssg";
import loadable from "@loadable/component";

const Loading = () => (
  <Layout><SEO title={'xxx'}/>Loading...</Layout>
);

const LoadableMap = loadable(() => import("../../components/SellerMap.jsx"), {
  fallback: <Loading />
});

export const BuyPage = () => {
  const { sellers } = useContent('/sellers.yml');
  if (typeof document === 'undefined') return <Loading/>;
  return (
    <Layout pure>
      <SEO title={'xxx'} />
      <div style={{ height: '58px' }}/>
      <h1 style={{
        position: 'absolute', top: '80px', zIndex: 500,
        textAlign: "center", width: '100%',
      }}>
        یکی از تولید کنندگان را انتخاب کنید.
      </h1>
      <LoadableMap sellers={sellers}/>
    </Layout>
  );
};
