import React from "react"

import { Layout } from "../../components/Layout.jsx"
import { SEO } from "../../components/SEO.jsx"
import { useContent } from "react-ssg";
import { useParams } from "react-router-dom";


export const SellerPage = () => {
  const { sellers } = useContent('/sellers.yml');
  const { id } = useParams();
  const s = sellers[id];
  return (
    <Layout>
      <SEO title={'xxx'} />
      <h1>{s.name}</h1>
      راه های ارتباطی:
      <ul>
        <li>
          تلگرام:
          <a dir="ltr" href={`https://t.me/${s.contact.telegram}`}>@{s.contact.telegram}</a>
        </li>
        <li>
          ماتریس:
          <a dir="ltr" href={`https://matrix.to/#/#${s.contact.matrix}`}>{`#${s.contact.matrix}`}</a>
        </li>
      </ul>
      <p>
      با این تهیه کننده تماس بگیرید تا اطلاعات بیشتر راجع به خرید محصول مورد نظرتان را
      دریافت کنید.
      </p>
      <p>
        <strong>
          این تهیه کننده (و هیچ تهیه کننده دیگری) هیچ ارتباطی با جمعیت داده های عمومی
          ندارند. مسئولیت خرید شما کاملا به عهده خود شماست.
        </strong>
      </p>
    </Layout>
  );
};
