import React from "react";
import { Button } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export const ItemCard = ({
  id, name, price, description, image = "/dist/static/images/main-background.svg"
}) => {
  return (
    <Card>
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
        <Button variant="primary" as={Link} to={`/items/${id}/`}>اطلاعات بیشتر</Button>
      </Card.Body>
      <Card.Footer>
        {price.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} تومان
      </Card.Footer>
    </Card>
  );
};