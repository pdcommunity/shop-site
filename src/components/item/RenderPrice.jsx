import React from "react";
import { Link } from "react-router-dom";

const numSeperator = (x) => x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ","); 

const priceNum = (o) => {
  const c = o.count ?? 1;
  if (c == 1) {
    return numSeperator(o.price);
  }
  return `${numSeperator(o.price)} * (${c} ${o.unit ?? 'عدد'}) = ${numSeperator(o.price * c)}`;
}

export const RenderPrice = ({ obj }) => {
  if (!obj) return <span>broken</span>;
  const p = priceNum(obj);
  const elem = obj.link ? (
    <a href={obj.link}>{obj.name}</a>
  ) : (
    <Link to={`../${obj.id}/`}>{obj.name}</Link>
  );
  if (obj.type === 'link' || obj.type === 'internal') {
    return (
      <li>
        {elem}: {p} (
        آخرین به روز رسانی در {
          new Date(obj.lastUpdate).toLocaleDateString('fa', { month: 'long', year: 'numeric', day: 'numeric' })
        } )
      </li>
    );
  }
  if (obj.type === 'const') {
    return (
      <li>{obj.name}: {p}</li>
    );
  }
  if (obj.type === 'group') {
    return (
      <li>
        {obj.name}: {p}
        <ul>
          {obj.list.map((k) => (
            <RenderPrice obj={k}/>
          ))}
        </ul>
      </li>
    );
  }
};
