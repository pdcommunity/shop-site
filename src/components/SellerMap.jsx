import React from "react"

import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import 'leaflet/dist/leaflet.css';
import 'react-leaflet-markercluster/dist/styles.min.css';
import MarkerClusterGroup from "react-leaflet-markercluster";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export const SellerMap = ({ sellers }) => {
  if (typeof document === 'undefined') return <div></div>;
  window.L.Icon.Default.imagePath = 'https://unpkg.com/leaflet@1.7.1/dist/images/';
  return (
    <MapContainer
      center={[34, 51.4111]} zoom={6} scrollWheelZoom={false}
      style={{ height: '80vh' }}
    >
      <TileLayer
        attribution='&copy; مشارکت کنندگان <a href="http://osm.org/copyright">اوپن استریت مپ</a> | <a href="https://openstreetmap.org/edit/">نقشه را بهبود دهید</a>'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <MarkerClusterGroup>
        {sellers.map((x, i) => (
          <Marker position={x.location}>
            <Popup>
              نام: {x.name} <br/>
              <Button
                style={{ color: 'white' }}
                size="sm" as={Link} to={`/buy/${i}`}>مشاهده</Button>
            </Popup>
          </Marker>
        ))}
      </MarkerClusterGroup>
    </MapContainer>
  );
};

export default SellerMap;
