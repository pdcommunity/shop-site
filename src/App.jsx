import React, { useEffect } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { BlogPage } from "./pages/blog/BlogPage.jsx";
import { FAQ } from "./pages/FAQ.jsx";
import { IndexPage } from "./pages/IndexPage.jsx";
import { NotFoundPage } from "./pages/NotFoundPage.jsx";
import { withRouter } from 'react-router-dom';
import { BlogIndex } from "./pages/blog/BlogIndex.jsx";
import { ItemPage } from "./pages/ItemPage.jsx";
import { BuyPage } from "./pages/seller/SellerMap.jsx";
import { SellerPage } from "./pages/seller/SellerPage.jsx";

const ScrollToTop = withRouter(({ history }) => {
  useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0);
    });
    return () => {
      unlisten();
    }
  }, []);

  return (null);
});

export const App = () => (
  <>
    <ScrollToTop/>
    <Switch>
      <Route path="/:url*" exact strict render={props => <Redirect to={`${props.location.pathname}/`}/>}/>
      <Route path="/" exact>
        <IndexPage/>
      </Route>
      <Route path="/items/:id">
        <ItemPage/>
      </Route>
      <Route path="/faq/:id">
        <FAQ/>
      </Route>
      <Route path="/faq/">
        <FAQ/>
      </Route>
      <Route path="/blogs/:id">
        <BlogPage/>
      </Route>
      <Route path="/blogs">
        <BlogIndex/>
      </Route>
      <Route path="/buy/:id">
        <SellerPage/>
      </Route>
      <Route path="/buy">
        <BuyPage/>
      </Route>
      <Route path="*">
        <NotFoundPage/>
      </Route>
    </Switch>
  </>
);
