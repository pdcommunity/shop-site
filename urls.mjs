const removeMD = (x)=>{
  if (x.endsWith('.md')) {
    return x.slice(0, -3) + "/";
  }
};

export const urls = (data) => [
  "/", "/faq/", "/blogs/", "/buy/",
  ...Object.keys(data).filter((x)=>x.startsWith('/blogs/')).map(removeMD),
  ...Object.keys(data).filter((x)=>x.startsWith('/items/')).map(removeMD),
];
