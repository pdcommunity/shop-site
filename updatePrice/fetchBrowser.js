import { readFile } from "fs/promises";
import { join } from "path";
import { rootFolder } from "../paths.mjs";
import pt from "puppeteer-core";

export const fetchBrowser = async () => {
  try {
    const { path } = JSON.parse(await readFile(join(rootFolder, 'updatePrice', 'data.json')));
    return { path, downloaded: false };  
  } catch(e) {
    console.log('Browser not found!');
    const bf = pt.createBrowserFetcher();
    const ri = await bf.download('870763');
    return { path: ri.executablePath, downloaded: true };
  }
};
