import puppeteer from "puppeteer-core";
import { fetchBrowser } from "./fetchBrowser.js";
import { vendors } from "./vendors.js";
import { readdir, readFile, writeFile } from "fs/promises";
import { join } from "path";
import { rootFolder } from "../paths.mjs";
import YAML from "yamljs";
import AwaitLock from "await-lock";

let browser;
const graph = new Map();
const now = new Date();

const lock = new AwaitLock.default();

const fetchLink = async (u, options) => {
  const url = new URL(u);
  const domain = url.hostname.replace('www.', '');
  const v = vendors.find((x)=>x.domain === domain);
  if (!v) {
    console.log(`Unknown vendor: ${domain}`);
    return 0;
  }
  await lock.acquireAsync();
  console.log(`start fetching ${u} with ${options}`);
  const page = await browser.newPage();
  let price = 0;
  try {
    await page.goto(u);
    price = await v.func(page, options);
  } catch(e) {
    
  }
  await page.close();
  lock.release();
  return price;
};

const completePrice = async (x) => {
  if (!x) {
    console.log('Can not calculate price of null');
    process.exit(0);
  }
  if (x.lastUpdate && now - x.lastUpdate < 3600_000 ) return;
  if (x.type === 'group') {
    console.log(x.list);
    await Promise.all(x.list.map(completePrice));
    x.price = x.list.map((a) => a.price * (a.count ?? 1)).reduce((a, b) => a+b);
  } else if (x.type === 'link') {
    const price = (await fetchLink(x.link, x.options)) ?? 0;
    if (price === 0) {
      if (!x.price) x.price = 0;
      return;
    }
    x.price = price;
  } else if (x.type === 'const') {
    return;
  } else if (x.type === 'internal') {
    const y = graph.get(`${x.id}.md`);
    if (!y) {
      console.log(`bad internal ${x.id}`);
      console.log(graph);
      process.exit(0);
    }
    await completePrice(y.price);
    x.price = y.price.price;
  } else {
    console.log(`Unsupported type ${x.type}`);
    console.log(x);
    process.exit(0);
  }
  if (!x.price) {
    console.log(`Something wrong ${x.type}`);
    console.log(x);
    process.exit(0);  
  }
  x.lastUpdate = now;
};

const main2 = async () => {
  const browserData = await fetchBrowser();
  browser = await puppeteer.launch({
    executablePath: browserData.path,
    args: browserData.downloaded ? ['--no-sandbox', '--disable-setuid-sandbox'] : [],
    headless: true,
  });
  console.log('Browser loaded...');
  const folder = join(rootFolder, 'content', 'items');
  const d = await readdir(folder);
  const x = await Promise.all(d.map(async (f) => {
    const p = join(folder, f);
    const sp = (await readFile(p)).toString().split('---');
    const y = YAML.parse(sp[1]);
    graph.set(f, y);
    return [p, y, sp[2]];
  }));
  await Promise.all(x.map(async ([f, y, txt]) => {
    if (typeof y.price === 'number') return;
    if (!y.price) {
      console.log('y does not have price');
      console.log(y);
      process.exit(0);
    }
    await completePrice(y.price);
    const full = `---\n${YAML.stringify(y, 1000, 2)}---${txt}`;
    await writeFile(f, full);
  }));
  await browser.close();
};

main2();
