import puppeteer from "puppeteer-core";

const delay = (ms) => new Promise((res)=>setTimeout(res, ms));
const p2e = (s) => s.replace(/[۰-۹]/g, d => '۰۱۲۳۴۵۶۷۸۹'.indexOf(d));

const localDelayForLoad = 5000;

/**
 * @type Array<{
 *  name: String,
 *  domain: String,
 *  func: (page: puppeteer.Page, options) => Promise<Number>
 * }>
 */
export const vendors = [
  {
    name: 'sanaat baazaar',
    domain: 'sanatbazar.com',
    func: async (page, options) => {
      if (options) {
        await page.evaluate((o) => {
          setAttrValue(o[0], o[1]);
        }, options);
        await delay(localDelayForLoad);
      }
      return page.evaluate(() => {
        const d = document.querySelector('span#block_price');
        return Number(d.innerHTML.replace(/[^0-9]/g, ''));
      });
    }
  },
  {
    name: 'digikala',
    domain: 'digikala.com',
    func: async (page) => {
      const t = await page.evaluate(() => {
        const d = document.querySelector('.c-product__seller-price-pure');
        return d.innerHTML;
      });
      return Number(p2e(t).replace(/[^0-9]/g, ''));
    }
  },
  {
    name: 'cafe robot',
    domain: 'thecaferobot.com',
    func: async (page) => {
      const t = await page.evaluate(() => {
        const d = document.querySelector('.price');
        return d.innerHTML;
      });
      return Number(p2e(t).replace(/[^0-9]/g, ''));
    }
  },
];
