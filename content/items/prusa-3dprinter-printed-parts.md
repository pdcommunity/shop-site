---
name: 'قطعات پرینت شده پرینتر سه بعدی پروسا'
price:
  type: group
  name: 'مجموع قیمت'
  list:
    -
      name: 'فیلامنت ABS با قطر ۱.۷۵'
      count: 0.6
      unit: کیلوگرم
      type: link
      link: 'https://www.digikala.com/product/dkp-1583240/%D9%81%DB%8C%D9%84%D8%A7%D9%85%D9%86%D8%AA-abs-%D9%BE%D8%B1%DB%8C%D9%86%D8%AA%D8%B1-%D8%B3%D9%87-%D8%A8%D8%B9%D8%AF%DB%8C-%D8%B1%D8%A7%DB%8C%DA%A9%D8%A7-%D9%85%D8%AF%D9%84-rk311-%D9%82%D8%B7%D8%B1-175-%D9%85%DB%8C%D9%84%DB%8C-%D9%85%D8%AA%D8%B1-1-%DA%A9%DB%8C%D9%84%D9%88%DA%AF%D8%B1%D9%85#suppliers'
      price: 230000
      lastUpdate: 2021-09-10T21:11:43.027Z
    -
      name: 'استهلاک پرینتر سه بعدی'
      count: 60
      unit: ساعت
      type: const
      price: 800
    -
      name: 'کارمزد نیروی انسانی'
      type: const
      price: 20000
  price: 206000
  lastUpdate: 2022-06-05T21:11:27.349Z
image: 'https://static.miraheze.org/pdcommunitywiki/0/0d/Prusa-i3-Y-axis-corner-20p.png'
description: 'تجهیزات لازم برای ساخت پرینتر پروسا، یک پرینتر سه بعدی رپرپ'
---

برای کیت کامل،
[کیت پرینتر سه بعدی پروسا](../prusa-3dprinter-kit/)
را ببینید.

پروسا یکی از بیشترین پرینتر های استفاده شده در جهان است. این پرینتر از خانواده
رپرپ می باشد.

این پرینتر به بیش از صد قطعه پرینت شده نیاز دارد که وزن آن ها روی هم 570 گرم
می شود. فایل های این قطعات در این لینک وجود دارند. شما این قطعات را با جنس
ABS
تحویل می گیرید که ارزان و در عین حال مقاوم است.
