---
name: 'کیت پرینتر سه بعدی پروسا'
price:
  type: group
  name: 'مجموع قیمت'
  list:
    -
      type: group
      name: 'قطعات الکترونیکی'
      list:
        -
          name: 'درایور استپر'
          type: link
          link: 'https://www.digikala.com/product/dkp-2211495/%D9%85%D8%A7%D8%B2%D9%88%D9%84-%D8%AF%D8%B1%D8%A7%DB%8C%D9%88%D8%B1-%D8%A7%D8%B3%D8%AA%D9%BE%D8%B1-%D9%85%D9%88%D8%AA%D9%88%D8%B1-%D9%85%D8%AF%D9%84-a4988'
          price: 40000
          count: 4
          lastUpdate: 2022-02-04T21:12:01.578Z
        -
          name: استپر
          type: const
          price: 130000
          count: 5
        -
          name: 'صفحه داغ- هیت بد پرینترهای سه بعدی RepRap مدل MK2B به همراه سیم'
          type: link
          link: 'https://thecaferobot.com/store/3d-printer-heated-bed-mk2'
          price: 184000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'منبع تغذیه 12V 20A 240W به همراه فن'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%85%D9%86%D8%A8%D8%B9_%D8%AA%D8%BA%D8%B0%DB%8C%D9%87_12_%D9%88%D9%84%D8%AA_20_%D8%A2%D9%85%D9%BE%D8%B1_240_%D9%88%D8%A7%D8%AA_%D8%A8%D9%87_%D9%87%D9%85%D8%B1%D8%A7%D9%87_%D9%81%D9%86'
          price: 340700
          lastUpdate: 2022-03-28T21:11:33.203Z
        -
          name: 'میکرو سوئیچ بسته 5 تایی'
          type: link
          link: 'https://thecaferobot.com/store/125v-2a-handle-micro-switch'
          price: 4900
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'شیلد رمپس ۱.۴'
          type: link
          link: 'https://thecaferobot.com/store/ramps-3dprinter-reprap'
          price: 145200
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'آردوینو مگا ۲۵۶۰'
          type: link
          link: 'https://thecaferobot.com/store/mega-2560-r3-arduino'
          price: 520000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'مقاومت حرارتی ( ترمیستور ) 100 کیلو اهم'
          type: link
          link: 'https://thecaferobot.com/store/ntc-thermistor-100k-1-3950-resistant-to-200-degrees-with-sucket'
          price: 15600
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'فن ۱۲ ولتی'
          type: link
          link: 'https://thecaferobot.com/store/40x40-fan-12v'
          price: 19500
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'هات اند Hotend پرینتر سه بعدی E3D V6 قطر نازل 0.4 قطر فیلامنت 1.75 میلی متر'
          type: link
          link: 'https://thecaferobot.com/store/3d-printer-e3d-v6-hotend-0-4mm-nozzle-1-75mm-filamnet'
          price: 30000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پوشش نگهدارنده به همراه فن 12 ولتی، مخصوص اکسترودرهای E3D V6'
          type: link
          link: 'https://thecaferobot.com/store/e3d-v5-e3d-v6-fan-cover-12v-3dprinter'
          price: 43100
          lastUpdate: 2022-06-05T21:11:27.349Z
      price: 2113000
      lastUpdate: 2022-06-05T21:11:27.349Z
    -
      type: group
      name: 'پیچ، مهره و قطعات جزئی'
      list:
        -
          name: 'شفت هارد کروم با قطر 8 میلی‌متر ۴۰ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%B4%D9%81%D8%AA-%D9%87%D8%A7%D8%B1%D8%AF-%DA%A9%D8%B1%D9%88%D9%85-%DA%86%DB%8C%D9%86%DB%8C-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 9
            - 363
          price: 73100
          count: 4
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ متری با قطر۱۰ میل ۱ متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%D9%85%D8%AA%D8%B1%DB%8C-%D9%82%D8%B7%D8%B1-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%B7%D9%88%D9%84-1-%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 2387
          count: 1
          price: 25300
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ متری با قطر ۸ میل ۱ متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%D9%85%D8%AA%D8%B1%DB%8C-%D9%82%D8%B7%D8%B1-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%B7%D9%88%D9%84-1-%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 797
          count: 1
          price: 18400
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'مهره آهنی ۱۰ میل'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%85%D9%87%D8%B1%D9%87-%D8%B3%D8%A7%D8%AF%D9%87-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-%D8%AF%D8%A7%D8%AE%D9%84%DB%8C-4-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 2387
          count: 12
          price: 1600
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'واشر ۱۰ میل'
          type: const
          price: 100
          count: 12
        -
          name: 'مهره آهنی ۸ میل'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%85%D9%87%D8%B1%D9%87-%D8%B3%D8%A7%D8%AF%D9%87-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-%D8%AF%D8%A7%D8%AE%D9%84%DB%8C-4-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 797
          count: 24
          price: 700
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'واشر تخت ساده استیل 8 میلی متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%88%D8%A7%D8%B4%D8%B1-%D8%AA%D8%AE%D8%AA-%D8%B3%D8%A7%D8%AF%D9%87-%D8%A7%D8%B3%D8%AA%DB%8C%D9%84-8-%D9%85%DB%8C%D9%84%DB%8C-%D9%85%D8%AA%D8%B1'
          count: 26
          price: 500
          lastUpdate: 2021-05-26T06:14:47.744Z
        -
          name: 'واشر پلاستیکی با قطر داخلی 3 میلی متر بسته 100 تایی'
          type: link
          link: 'https://thecaferobot.com/store/polymer-washer'
          price: 400
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ آلن استیل 8 میلیمتر با طول ۳ سانت'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%D8%A2%D9%84%D9%86-%D8%A7%D8%B3%D8%AA%DB%8C%D9%84-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%A8%D8%A7-%D8%B7%D9%88%D9%84-%D9%87%D8%A7%DB%8C-%D9%85%D8%AE%D8%AA%D9%84%D9%81-%D8%B7%D9%88%D9%84-%D8%B1%D8%A7-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%DA%A9%D9%86%DB%8C%D8%AF'
          options:
            - 9
            - 15
          count: 2
          price: 9600
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'LM8UU بلبرینگ'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%A8%D9%84%D8%A8%D8%B1%DB%8C%D9%86%DA%AF-%D8%AE%D8%B7%DB%8C-lm6uu-%D8%AF%D8%B1%D8%AC%D9%87-2'
          options:
            - 56
            - 963
          count: 10
          price: 18000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'بلبرینگ 608zz  قطر خارجی 22 قطر داخلی 8 پهنا 7 میلی متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%A8%D9%84%D8%A8%D8%B1%DB%8C%D9%86%DA%AF-608-%D9%82%D8%B7%D8%B1-%D8%AE%D8%A7%D8%B1%D8%AC%DB%8C-22-%D9%82%D8%B7%D8%B1-%D8%AF%D8%A7%D8%AE%D9%84%DB%8C-8-%D9%BE%D9%87%D9%86%D8%A7-7-%D9%85%DB%8C%D9%84%DB%8C-%D9%85%D8%AA%D8%B1'
          options:
            - 60
            - 2913
          count: 5
          price: 21000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پولی تایمینگ جی تی ۲'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%D9%88%D9%84%DB%8C-%D8%AA%D8%A7%DB%8C%D9%85%DB%8C%D9%86%DA%AF-36gt2-%D8%A8%D8%A7-%D8%B9%D8%B1%D8%B6-6-%D9%85%DB%8C%D9%84%DB%8C-%D9%85%D8%AA%D8%B1-%D9%88-%DA%AF%D8%A7%D9%85-2-032-%D9%85%DB%8C%D9%84%DB%8C-%D9%85%D8%AA%D8%B1'
          options:
            - 63
            - 1442
          count: 2
          price: 24000
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'تسمه تایمینگ متری GT2 با گام 2 میلی‌متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%AA%D8%B3%D9%85%D9%87-gt2-%D8%A8%D8%A7-%DA%AF%D8%A7%D9%85-2-5-%D8%A8%D8%A7-%D8%B9%D8%B1%D8%B6-6-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D9%85%D8%AA%D8%B1%DB%8C'
          options:
            - 36
            - 1389
          count: 2
          price: 36100
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'یک تکه شیشه 20,25 در 21,5 سانتی متر'
          type: const
          price: 5000
        -
          name: 'یک بسته گیره کاغذ'
          type: const
          price: 40000
        -
          name: 'پیچ ام دی اف ۳ میل ۲ سانتی'
          type: const
          price: 1000
          count: 20
        -
          name: 'پیچ چهارسو استیل M3 با قطر 3 میلیمتر ۱ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%DA%86%D9%87%D8%A7%D8%B1%D8%B3%D9%88-3-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%A8%D8%A7-%D8%B7%D9%88%D9%84-%D9%87%D8%A7%DB%8C-%D9%85%D8%AE%D8%AA%D9%84%D9%81-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%DA%A9%D9%86%DB%8C%D8%AF'
          options:
            - 9
            - 13
          count: 17
          price: 900
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ چهارسو استیل M3 با قطر 3 میلیمتر ۲.۵ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%DA%86%D9%87%D8%A7%D8%B1%D8%B3%D9%88-3-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%A8%D8%A7-%D8%B7%D9%88%D9%84-%D9%87%D8%A7%DB%8C-%D9%85%D8%AE%D8%AA%D9%84%D9%81-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%DA%A9%D9%86%DB%8C%D8%AF'
          options:
            - 9
            - 41
          count: 7
          price: 1200
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ چهارسو استیل M3 با قطر 3 میلیمتر ۳ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%DA%86%D9%87%D8%A7%D8%B1%D8%B3%D9%88-3-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%A8%D8%A7-%D8%B7%D9%88%D9%84-%D9%87%D8%A7%DB%8C-%D9%85%D8%AE%D8%AA%D9%84%D9%81-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%DA%A9%D9%86%DB%8C%D8%AF'
          options:
            - 9
            - 15
          count: 18
          price: 1700
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ چهارسو استیل M3 با قطر 3 میلیمتر ۴ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%DA%86%D9%87%D8%A7%D8%B1%D8%B3%D9%88-3-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%A8%D8%A7-%D8%B7%D9%88%D9%84-%D9%87%D8%A7%DB%8C-%D9%85%D8%AE%D8%AA%D9%84%D9%81-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%DA%A9%D9%86%DB%8C%D8%AF'
          options:
            - 9
            - 45
          count: 1
          price: 3300
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'مهره آهنی ۳ میل'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%85%D9%87%D8%B1%D9%87-%D8%B3%D8%A7%D8%AF%D9%87-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-%D8%AF%D8%A7%D8%AE%D9%84%DB%8C-4-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 1168
          count: 26
          price: 300
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'مهره آهنی ۵ میل'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%85%D9%87%D8%B1%D9%87-%D8%B3%D8%A7%D8%AF%D9%87-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-%D8%AF%D8%A7%D8%AE%D9%84%DB%8C-4-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 657
          count: 2
          price: 440
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'فنر فشاری هیت بد'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%81%D9%86%D8%B1_%D9%81%D8%B4%D8%A7%D8%B1%DB%8C_%D8%A8%D8%A7_%D8%B7%D9%88%D9%84_15_%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1_%D9%82%D8%B7%D8%B1_%D8%AF%D8%A7%D8%AE%D9%84%DB%8C_17_%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          count: 4
          price: 9500
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'بست زیپی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%A8%D8%B3%D8%AA-%D8%B2%DB%8C%D9%BE%DB%8C-2mm-100mm-%D8%A8%D8%B3%D8%AA%D9%87-100-%D8%AA%D8%A7%DB%8C%DB%8C-%D8%AF%D8%A7%D8%B1%D8%A7%DB%8C-%D8%AA%DA%AF-%D9%84%DB%8C%D8%A8%D9%84-%D8%AC%D8%AF%D8%A7'
          price: 11100
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'شفت هارد کروم با قطر 8 میلی‌متر ۵۰ سانتی'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D8%B4%D9%81%D8%AA-%D9%87%D8%A7%D8%B1%D8%AF-%DA%A9%D8%B1%D9%88%D9%85-%DA%86%DB%8C%D9%86%DB%8C-%D8%A8%D8%A7-%D9%82%D8%B7%D8%B1-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1'
          options:
            - 9
            - 365
          count: 2
          price: 91200
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: 'پیچ متری با قطر ۵ میل ۱ متر'
          type: link
          link: 'https://www.sanatbazar.com/shop/shop-main/%D9%BE%DB%8C%DA%86-%D9%85%D8%AA%D8%B1%DB%8C-%D9%82%D8%B7%D8%B1-8-%D9%85%DB%8C%D9%84%DB%8C%D9%85%D8%AA%D8%B1-%D8%B7%D9%88%D9%84-1-%D9%85%D8%AA%D8%B1'
          options:
            - 48
            - 657
          count: 1
          price: 12700
          lastUpdate: 2022-06-05T21:11:27.349Z
        -
          name: '625zz بلبرینگ'
          type: const
          price: 20000
          count: 2
        -
          name: 'پیچ دندانه دار اکسترودر'
          type: const
          price: 50000
      price: 1276580
      lastUpdate: 2022-06-05T21:11:27.349Z
    -
      name: 'قطعات پرینت شده'
      type: internal
      id: prusa-3dprinter-printed-parts
      price: 206000
      lastUpdate: 2022-06-05T21:11:27.349Z
  price: 3595580
  lastUpdate: 2022-06-05T21:11:27.349Z
description: 'یک پرینتر سه بعدی رپرپ'
image: 'https://static.miraheze.org/pdcommunitywiki/thumb/7/72/30a.jpg/300px-30a.jpg'
---

پروسا یکی از بیشترین پرینتر های استفاده شده در جهان است. این پرینتر از خانواده
رپرپ می باشد.

در این کیت شما
[این قطعات](https://index.pdcommunity.ir/wiki/%D9%BE%D8%B1%D9%88%D8%B3%D8%A7_%D8%A2%DB%8C_%DB%B3#%D8%AA%D8%AC%D9%87%DB%8C%D8%B2%D8%A7%D8%AA_%D9%84%D8%A7%D8%B2%D9%85)
را دریافت خواهید کرد.

آموزش ساخت این پرینتر در
[این لینک](https://index.pdcommunity.ir/wiki/%D9%BE%D8%B1%D9%88%D8%B3%D8%A7_%D8%A2%DB%8C_%DB%B3)
وجود دارد.
