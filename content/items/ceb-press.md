---
name: 'دستگاه پرس کب'
price:
  type: const
  name: 'مجموع قیمت'
  price: 100000000
description: 'دستگاه ساخت کب (بلوک زمین فشرده) یک مصالح ساختمانی مدرن و در دسترس'
---

توجه: ساخت این دستگاه به زمان زیادی نیاز دارد و هر توزیع کننده ای تجهیزات مورد نیاز
آن را ندارد.

جزییات بیشتر و نخوه ساخت این دستگاه را در
[این لینک](https://index.pdcommunity.ir/wiki/%D8%AF%D8%B3%D8%AA%DA%AF%D8%A7%D9%87_%D9%BE%D8%B1%D8%B3_%DA%A9%D8%A8)
ببینید.

